#Author: lamartin@bancolombia.com
#language: es
@tag
Característica: Registro Demo actualizacion
  Yo quiero registrarme al sistema para poder hacer uso de funcionalidades

@GoodWay
  Escenario: Registro de usuario pagina Demo
    Dado que Carlos quiere acceder a la Web Automation Demo Site
    Cuando el realiza el registro en la pagina
   |name  |apellido|direccion |email        |telefono  |Pais     |AñoNacimiento|MesNacimiento|DiaNacimiento|Contrasena|ConfirmarContrasena|
   |Martin|Martinez|calle 31 B|prueba@prueba|3136340779|Austria  |1991         |November     |26           |Martin2017|Martin2017         |
   Entonces el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row
