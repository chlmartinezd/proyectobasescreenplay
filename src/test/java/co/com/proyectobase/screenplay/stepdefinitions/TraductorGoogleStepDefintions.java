package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor andrea=Actor.named("Andrea");
	
	@Before
	public void ConfiguracionInicial()
	{
		andrea.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^que Andrea quiere usar el traductor de google$")
	public void queAndreaQuiereUsarElTraductorDeGoogle()  {
	  andrea.wasAbleTo(Abrir.laPaginaDeGoogle());
	}

	@When("^el traduce la palabra (.*) de Ingles a Español$")
	public void elTraduceLaPalabraLoveDeInglesAEspañol(String palabra) {
	  andrea.attemptsTo(Traducir.DeInglesAEspanol(palabra));
	}

	@Then("^el deberia ver la palabra (.*) en la pantalla$")
	public void elDeberiaVerLaPalabraAmorEnLaPantalla(String palabraesperada)  {
	   andrea.should(seeThat(LaRespuesta.es(),equalTo(palabraesperada)));
	}

}
