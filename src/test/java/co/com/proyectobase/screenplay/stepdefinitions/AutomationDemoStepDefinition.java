package co.com.proyectobase.screenplay.stepdefinitions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.VerificaRegistro;
import co.com.proyectobase.screenplay.tasks.AbrirR;
import co.com.proyectobase.screenplay.tasks.Registrarenlapagina;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.*;

public class AutomationDemoStepDefinition {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor andrea=Actor.named("Andrea");
	

	@Before
	public void ConfiguracionInicial()
	{
		andrea.can(BrowseTheWeb.with(hisBrowser));
	}

	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite()  {
		andrea.wasAbleTo(AbrirR.LaPaginadeAutomationDemo());
	}

	@Cuando("^el realiza el registro en la pagina$")
	public void elRealizaElRegistroEnLaPagina(DataTable arg1)  {
		
		andrea.attemptsTo(Registrarenlapagina.conlaInfo(arg1));
 
	}

	@Entonces("^el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow()  {
		andrea.should(GivenWhenThen.seeThat(VerificaRegistro.verificarTexto()));
	}
	
}
