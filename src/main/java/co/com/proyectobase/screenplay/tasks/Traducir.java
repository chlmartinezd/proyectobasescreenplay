package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.GoogleHomePage;
import co.com.proyectobase.screenplay.userinterface.GoogleTraductorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Traducir implements Task {
	
	public Traducir(String palabra) {
		super();
		this.palabra = palabra;
	}

	private String palabra;

	@Override
	public <T extends Actor> void performAs(T andrea) {
	
		///Ingreso a la opción
		andrea.attemptsTo(Click.on(GoogleHomePage.BOTON_APLICACIONES));
		andrea.attemptsTo(Click.on(GoogleHomePage.BOTON_GOOGLE_TRANSLATE));
		
		//Ingreso de palabra a traducir
		
		andrea.attemptsTo(Click.on(GoogleTraductorPage.BOTON_LENGUAJE_ORIGEN));
		andrea.attemptsTo(Click.on(GoogleTraductorPage.OPCION_INGLES));
		andrea.attemptsTo(Click.on(GoogleTraductorPage.BOTON_LENGUAJE_DESTINO));
		andrea.attemptsTo(Click.on(GoogleTraductorPage.OPCION_ESPANOL));
		andrea.attemptsTo(Enter.theValue(palabra).into(GoogleTraductorPage.AREA_DE_TRADUCCION));
		
		andrea.attemptsTo(Click.on(GoogleTraductorPage.BOTON_TRADUCIR));
		
	}
	
	public static Traducir DeInglesAEspanol(String palabra) {
		
		return Tasks.instrumented(Traducir.class,palabra);
	}

}
