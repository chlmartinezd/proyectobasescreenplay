package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.AutomationDemoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirR implements Task {

	private AutomationDemoPage automationDemoPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(automationDemoPage));	
		
	}

	public static AbrirR LaPaginadeAutomationDemo() {
		
		return Tasks.instrumented(AbrirR.class);
	}

}
