package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.userinterface.AutomationDemoPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


public class Registrarenlapagina implements Task{
	
	private DataTable dtAutDem;
	
	
	public Registrarenlapagina(DataTable dtAutDem) {
		super();
		this.dtAutDem = dtAutDem;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		List<List<String>> data = dtAutDem.raw();
		
		actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(AutomationDemoPage.FirstName));
		actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(AutomationDemoPage.LastName));
		actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(AutomationDemoPage.Direccion));
		actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(AutomationDemoPage.Email));
		actor.attemptsTo(Enter.theValue(data.get(1).get(4).trim()).into(AutomationDemoPage.Telefono));
		actor.attemptsTo(Click.on(AutomationDemoPage.rdbGenero));
		actor.attemptsTo(Click.on(AutomationDemoPage.chk));
		actor.attemptsTo(Click.on(AutomationDemoPage.chk2));
		//actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(AutomationDemoPage.Lenguaje));
		//actor.attemptsTo(Enter.theValue(data.get(1).get(5).trim()).into(AutomationDemoPage.Habilidades));
		//actor.attemptsTo(SelectFromOptions.byIndex(Integer.parseInt(data.get(1).get(5).trim())).from(AutomationDemoPage.Countries2))));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(1).get(5).trim()).from(AutomationDemoPage.Countries));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(1).get(6).trim()).from(AutomationDemoPage.Year));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(1).get(7).trim()).from(AutomationDemoPage.Mes));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(1).get(8).trim()).from(AutomationDemoPage.Dia));
		actor.attemptsTo(Enter.theValue(data.get(1).get(9).trim()).into(AutomationDemoPage.Password));
		actor.attemptsTo(Enter.theValue(data.get(1).get(10).trim()).into(AutomationDemoPage.PasswordC));
		actor.attemptsTo(Click.on(AutomationDemoPage.Enviar));
	}

	public static Registrarenlapagina conlaInfo(DataTable dtAutDem) {
		return Tasks.instrumented(Registrarenlapagina.class, dtAutDem );
	}




	
	
	

}
