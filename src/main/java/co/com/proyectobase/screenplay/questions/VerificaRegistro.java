package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.AutomationDemoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Visibility;

public class VerificaRegistro implements Question <Boolean> {

	
	@Override
	public Boolean answeredBy(Actor andrea) {
		
		return Visibility.of(AutomationDemoPage.Label_Confirmar).viewedBy(andrea).asBoolean();
	}
	
	public static VerificaRegistro verificarTexto() {
		
		return new VerificaRegistro();
	}

}
