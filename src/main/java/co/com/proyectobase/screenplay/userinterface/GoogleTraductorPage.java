package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

//*[@id="gt-sl-gms"]/div[2]
//*[@id="gt-tl-gms"]
public class GoogleTraductorPage {
	
	public static final Target BOTON_LENGUAJE_ORIGEN = Target.the("El botón para seleccionar lenguaje de origen").located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO = Target.the("El botón para seleccionar el lenguaje destino").located(By.id("gt-tl-gms"));
	
	public static final Target OPCION_INGLES = Target.the("Opción origen").located(By.xpath("//*[@id=\"gt-sl-gms-menu\"]//div[contains(text(),'nglés')]" ));
	public static final Target OPCION_ESPANOL = Target.the("Opción idioma espanol").located(By.xpath("//*[@id=\"gt-tl-gms-menu\"]//div[contains(text(),'Español')]"));
//	"//div[@id='gt-tl-gms-menu']/table/tbody//tr/td//div[contains(text(),'espa')]"
	public static final Target AREA_DE_TRADUCCION  = Target.the("El area de traducción").located(By.id("source"));
	public static final Target BOTON_TRADUCIR = Target.the("El botón para traducir").located(By.id("gt-submit"));
	public static final Target AREA_TRADUCIDA = Target.the("El area traducida").located(By.id("gt-res-dir-ctr"));
	
}
