package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.*;
import net.serenitybdd.core.annotations.findby.By;



@DefaultUrl("https://www.google.com")
public class GoogleHomePage extends PageObject{
	//*[@id="gbwa"]/div[1]/a
	//*[@id="gb51"]/span[1]
	public static final Target BOTON_APLICACIONES = Target.the("El botón que muestra las aplicaciones").located(By.id("gbwa"));
	public static final Target BOTON_GOOGLE_TRANSLATE = Target.the("Botón de app Traductor").located(By.id("gb51"));
}

